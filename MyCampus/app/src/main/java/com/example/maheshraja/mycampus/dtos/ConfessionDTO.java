package com.example.maheshraja.mycampus.dtos;

/**
 * Created by lenovo on 15-07-2016.
 */
public class ConfessionDTO implements DTO {

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    String comment;
    public int getConfession_id() {
        return confession_id;
    }

    public void setConfession_id(int confession_id) {
        this.confession_id = confession_id;
    }

    int confession_id;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    int code;

    public String getCollege_name() {
        return college_name;
    }

    public void setCollege_name(String college_name) {
        this.college_name = college_name;
    }

    String college_name;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    String data;


    public int getLikecount() {
        return likecount;
    }

    public void setLikecount(int likecount) {
        this.likecount = likecount;
    }

    int likecount;

}
