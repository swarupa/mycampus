package com.example.maheshraja.mycampus.Activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.utility.MyConstants;

import java.util.HashMap;
import java.util.Stack;

public class DangerActivity extends AppCompatActivity  implements FragmentDrawer.FragmentDrawerListener {
    TextView t1;
    protected MainActivity mActivity;
    private static String TAG = MainActivity.class.getSimpleName();

    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;


    /* A HashMap of stacks, where we use tab identifier as keys.. */
    public HashMap<String, Stack<BaseFragment>> mStacks;
    private String mCurrentTab;

    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_danger);
        t1 = (TextView) findViewById(R.id.textview);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);

        actionBar = getSupportActionBar();

        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

      /*  BitmapDrawable background = new BitmapDrawable(BitmapFactory.decodeResource(getResources(), R.drawable.toolbar_logo));
        actionBar.setBackgroundDrawable(background);*/

    /*    mStacks = new HashMap<String, Stack<BaseFragment>>();
        mStacks.put(MyConstants.TAB_HOME, new Stack<BaseFragment>());
        mStacks.put(MyConstants.TAB_EVENTS, new Stack<BaseFragment>());
        mStacks.put(MyConstants.TAB_Coupons, new Stack<BaseFragment>());
        mStacks.put(MyConstants.TAB_SPONSERS, new Stack<BaseFragment>());
        mStacks.put(MyConstants.TAB_SPONSERS, new Stack<BaseFragment>());
        mStacks.put(MyConstants.TAB_DASHBOARD, new Stack<BaseFragment>());

        mStacks.put(MyConstants.TAB_HELP, new Stack<BaseFragment>());
        mStacks.put(MyConstants.TAB_SETTINGS, new Stack<BaseFragment>());*/

        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);

        // display the first navigation drawer view on app launch
        //displayView(0);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        drawerFragment.updateProfilePic();
        //displayView(position);
        displayView(0);


    }

    private void displayView(int position) {
        BaseFragment fragment = null;
        String title = getString(R.string.app_name);

        switch (position) {
            case 0:
                mActivity.pushFragments(MyConstants.TAB_HOME, new AboutApp2Fragment(), false, true);
                break;
            case 1:

                mActivity.pushFragments(MyConstants.TAB_HOME, new EventsFragment(), false, true);
                break;
            case 2:
                mCurrentTab = MyConstants.TAB_Coupons;
                mStacks.get(mCurrentTab).clear();
                fragment = new CouponsFragment();
                pushFragments(mCurrentTab, fragment, false, true);
                title = getString(R.string.title_coupons);
                break;
            case 3:
                mCurrentTab = MyConstants.TAB_SPONSERS;
                mStacks.get(mCurrentTab).clear();
                fragment = new SponsorsFragment();
                pushFragments(mCurrentTab, fragment, false, true);
                title = getString(R.string.title_sponsors);
                break;

            /*case 4:
                mCurrentTab = MyConstants.TAB_DASHBOARD;
                mStacks.get(mCurrentTab).clear();
                fragment = new DashboardFragment();
                pushFragments(mCurrentTab, fragment, false, true);
                title = getString(R.string.title_dashboard);
                break;*/


            case 4:
                mCurrentTab = MyConstants.TAB_LOGOUT;
                mStacks.get(mCurrentTab).clear();
                fragment = new DashboardFragment();
                pushFragments(mCurrentTab, fragment, false, true);
                title = getString(R.string.title_logout);
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

            // set the toolbar title
            //getSupportActionBar().setTitle(title);
        }

    }


    public void pushFragments(String tag, BaseFragment fragment, boolean shouldAnimate, boolean shouldAdd) {
        if (shouldAdd)
            mStacks.get(tag).push(fragment);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        /*
		 * if(shouldAnimate) ft.setCustomAnimations(R.anim.slide_in_right,
		 * R.anim.slide_out_left);
		 */
        ft.replace(R.id.container_body, fragment);

        ft.commit();

    }

    public void popFragments() {
		/*
		 * Select the second last fragment in current tab's stack.. which will
		 * be shown after the fragment transaction given below
		 */
        Fragment fragment = mStacks.get(mCurrentTab).elementAt(
                mStacks.get(mCurrentTab).size() - 2);

		/* pop current fragment from stack.. */
        mStacks.get(mCurrentTab).pop();

		/*
		 * We have the target fragment in hand.. Just show it.. Show a standard
		 * navigation animation
		 */
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        // ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        ft.replace(R.id.container_body, fragment);
        ft.commit();
    }


}

