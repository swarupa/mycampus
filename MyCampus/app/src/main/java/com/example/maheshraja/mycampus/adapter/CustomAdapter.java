package com.example.maheshraja.mycampus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.maheshraja.mycampus.R;

public class CustomAdapter extends BaseAdapter {
    private Context mContext;

  /*  // Keep all Images in array
    public Integer[] mThumbIds = {
            R.drawable.pic_1, R.drawable.pic_2,
            R.drawable.pic_3, R.drawable.pic_4,
            R.drawable.pic_5, R.drawable.pic_6,
            R.drawable.pic_7, R.drawable.pic_8,
            R.drawable.pic_9, R.drawable.pic_10,
            R.drawable.pic_11, R.drawable.pic_12,
            R.drawable.pic_13, R.drawable.pic_14,
            R.drawable.pic_15
    };*/

    public  String[] names = {"IShare","Police","Confessions","TGS Jobs","Learning","TechGenius",};
    public  String[] name = {"Sharing Platform","Help"," Share Yourself","Job Portal","Online Learning","Consulting"};
    public  Integer[] namesBgColor = {R.color.transparent_white,R.color.transparent_white,R.color.transparent_white,R.color.transparent_white,R.color.transparent_white,R.color.transparent_white,R.color.transparent_white,R.color.transparent_white,R.color.transparent_white};

    // Constructor
    public CustomAdapter(Context c){
        mContext = c;
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object getItem(int position) {
        return names[position];
    }



    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder=null;
        if(convertView==null)
        {
            convertView= LayoutInflater.from(mContext).inflate(R.layout.grid_child,parent,false);
            viewHolder=new ViewHolder();
            viewHolder.content = (TextView)convertView.findViewById(R.id.maincontent);
            viewHolder.subcontent = (TextView)convertView.findViewById(R.id.footercontent);
            viewHolder.linearLayout = (LinearLayout)convertView.findViewById(R.id.linearPArent);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder= (ViewHolder) convertView.getTag();
        }
        viewHolder.content.setText(names[position]);
        viewHolder.subcontent.setText(name[position]);
        viewHolder.linearLayout.setBackgroundResource(namesBgColor[position]);

        return convertView;
    }

    private class ViewHolder
    {
        TextView content;
        TextView subcontent;
        LinearLayout linearLayout;
    }
}
