package com.example.maheshraja.mycampus.Activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.maheshraja.mycampus.R;

public class LogoutFragment extends BaseFragment {
    TextView text;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_logout, container, false);
        Toast.makeText(getContext(),"Logout Clicking",Toast.LENGTH_LONG).show();
        text = (TextView) rootView.findViewById(R.id.txttest);
        return rootView;
    }


}
