package com.example.maheshraja.mycampus.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.asynctask.MyAsyncTask;
import com.example.maheshraja.mycampus.daos.UserDAO;
import com.example.maheshraja.mycampus.database.DBHandler;
import com.example.maheshraja.mycampus.dtos.DTO;
import com.example.maheshraja.mycampus.dtos.UserDTO;
import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.DistanceCalculationManager;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


public class PoliceFragment extends BaseFragment {
    ListView listview;
    private Context context;
    ImageView image;
    EditText msg;
    String msg1;
    String url = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Utility.checkPlayServices(mActivity)) {
            buildGoogleApiClient();
        }

    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_ishare, container, false);

        listview = (ListView) rootView.findViewById(R.id.list_view);
        //image = (ImageView) rootView.findViewById(R.id.imageview1);
        listview.setAdapter(new ListAdapter(rootView.getContext()));

        context = mActivity;
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getContext(), "Item first postion is:" + position, Toast.LENGTH_LONG).show();
                switch (position) {
                    case 0:
                        //mActivity.pushFragments(MyConstants.TAB_HOME, new SponsorsFragment(), false, true);
                        break;

                }

            }
        });


        return rootView;
    }


    public class ListAdapter extends BaseAdapter {
        private Context mContext;

  /*  // Keep all Images in array
    public Integer[] mThumbIds = {
            R.drawable.pic_1, R.drawable.pic_2,
            R.drawable.pic_3, R.drawable.pic_4,
            R.drawable.pic_5, R.drawable.pic_6,
            R.drawable.pic_7, R.drawable.pic_8,
            R.drawable.pic_9, R.drawable.pic_10,
            R.drawable.pic_11, R.drawable.pic_12,
            R.drawable.pic_13, R.drawable.pic_14,
            R.drawable.pic_15
    };*/


        public String[] names = {"If you are in danger", "If you are suffering from any problem", "If you want to report an incident","If you want upload video","If you want to upload Audio" };

        public Integer[] namesBgColor = {R.color.transparent_white, R.color.transparent_white, R.color.transparent_white, R.color.transparent_white,R.color.transparent_white};
        public String[] name = {"Please Click Here......", "Please Click Here......", "Please Click Here......","Please Click Here","Please Click Here"};

        // Constructor
        public ListAdapter(Context c) {
            mContext = c;
        }

        @Override
        public int getCount() {
            return names.length;
        }

        @Override
        public Object getItem(int position) {
            return names[position];
        }


        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.list_item, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.content = (TextView) convertView.findViewById(R.id.maincontent);
                viewHolder.subtextview = (TextView) convertView.findViewById(R.id.subtextview);
                viewHolder.subcontent = (Button) convertView.findViewById(R.id.footercontent);
                viewHolder.linearLayout = (LinearLayout) convertView.findViewById(R.id.linearPArent);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.content.setText(names[position]);
            viewHolder.subtextview.setText(name[position]);
            viewHolder.linearLayout.setBackgroundResource(namesBgColor[position]);

            viewHolder.subcontent.setOnClickListener(new View.OnClickListener() {
                final String temp = (String) getItem(position);

                @Override
                public void onClick(View v) {


                    int position = (Integer) v.getTag();
                    System.out.println("Button clicked" + position);

                    switch (position) {
                        case 0:
                            showAlertForDanger();
                            break;
                        case 1:
                            mActivity.pushFragments(MyConstants.TAB_HOME, new SurakshaFragment(), false, true);
                            break;
                        case 2:
                            mActivity.pushFragments(MyConstants.TAB_HOME, new IncidientFragment(), false, true);
                            break;

                        case 3:
                            Intent phoneIntent3 = new Intent(getContext(),AndroidVideoCapture.class);

                            startActivity(phoneIntent3);
                            break;

                        case 4:
                            Intent phoneIntent = new Intent(getContext(),AudioRecordActivity.class);

                            startActivity(phoneIntent);
                            break;
                    }


                }
            });
            viewHolder.subcontent.setTag(position);
            return convertView;
        }

        private class ViewHolder {
            TextView content;
            TextView subtextview;
            Button subcontent;
            LinearLayout linearLayout;

        }


    }

    public void showAlertForDanger() {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.confirmation_popup);
        dialog.show();

        TextView alertMsg = (TextView) dialog.findViewById(R.id.alert_msg_text);
        alertMsg.setText("This will identify near by police personnal and report your status ?\n" + "They may come for your help?");
        msg = (EditText) dialog.findViewById(R.id.alert_msg);
        Button btnYes = (Button) dialog.findViewById(R.id.alert_yes);
        Button btnNo = (Button) dialog.findViewById(R.id.alert_no);

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                url = MyConstants.DANGER_URL;

                if (msg.getText().toString().trim().length() > 0) {
                    msg1 = getMessageForRagging(msg.getText().toString().trim());
                    senddata();
                } else {
                    Utility.showAlert(mActivity, null, "Please enter your message");
                }

            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();

            }
        });
    }


    private void senddata() {
        if (url != null) {
            if (location != null) {

                DistanceCalculationManager distMngr = new DistanceCalculationManager();

                MultipleSMS(distMngr.getSHOCellNumber(distMngr.getNearestSHO(Double.valueOf(location.split(",")[0]), Double.valueOf(location.split(",")[1]))), msg1);

                if (Utility.networkAvailability(mActivity)) {
                    String reqData = getRequestData(msg.getText().toString().trim());
                    MyAsyncTask asyncTask = new MyAsyncTask(reqData, onAsyncComplete, MyConstants.CONNECTION_TIMEOUT, MyConstants.SOCKET_TIMEOUT);
                    Utility.showProgressDialog(mActivity);
                    asyncTask.execute(url);
                } else {
                    Utility.showAlert(mActivity, null, "Please check your network connection");
                }
            } else {
                getCurrentLocation(mActivity);
            }
        } else {
            Utility.showAlert(mActivity, null, "Select your complaint type");
        }
    }


    private void MultipleSMS(final String phoneNumber, final String message) {

        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(mActivity, 0, new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(mActivity, 0, new Intent(DELIVERED), 0);


// For when the SMS has been sent
        mActivity.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        /*ContentValues values = new ContentValues();
                            for (int i = 0; i < MobNumber.size() - 1; i++) {
                                values.put("address", MobNumber.get(i).toString());
                                // txtPhoneNo.getText().toString());
                                values.put("body", message);
                            }
                            mActivity.getContentResolver().insert(Uri.parse("content://sms/sent"), values);*/
                        Toast.makeText(context, "SMS sent successfully", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(context, "Generic failure cause", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(context, "Service is currently unavailable", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(context, "No pdu provided", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(context, "Radio was explicitly turned off", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

// For when the SMS has been delivered
        mActivity.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(mActivity.getBaseContext(), "SMS delivered", Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(mActivity.getBaseContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
    }

    public void onStart() {
        super.onStart();
        if ((mGoogleApiClient != null) && (!mGoogleApiClient.isConnected())) {
            mGoogleApiClient.connect();
        }
    }

    public void onStop() {
        super.onStop();
        if ((mGoogleApiClient != null) && (mGoogleApiClient.isConnected())) {
            mGoogleApiClient.disconnect();
        }
    }

    private String getRequestData(String msg) {
        String userId;
        List<DTO> users = UserDAO.getInstance().getRecords(DBHandler.getInstance(mActivity).getDBObject(0));
        if (users == null || users.size() == 0)
            userId = "";
        else {
            UserDTO user = (UserDTO) users.get(0);
            userId = user.getEmailId();
        }
        JSONObject object = new JSONObject();
        try {
            object.put("deviceId", Utility.getDeviceId(mActivity));
            object.put("latLong", location);

            UserDTO localObject = (UserDTO) UserDAO.getInstance().getRecords(DBHandler.getInstance(this.mActivity).getDBObject(0)).get(0);
            String str = "" + localObject.getName();
            str = str + "/" + localObject.getAge();
            str = str + "" + localObject.getGender().charAt(0);
            str = str + "\nMNo:" + localObject.getMobileNo();

            object.put("message", str + "##" + msg);
            DistanceCalculationManager distMngr = new DistanceCalculationManager();
            object.put("shoCell", distMngr.getSHOCellNumber(distMngr.getNearestSHO(Double.valueOf(location.split(",")[0]), Double.valueOf(location.split(",")[1]))));
            object.put("userId", userId);
            object.put("authcode", Utility.getAuthCode(mActivity));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return object.toString();
    }

    private AsyncTaskCompleteListener onAsyncComplete = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result) {
            if (result != null) {
                if (Utility.isJSONValid(result)) {
                    try {
                        JSONObject object = new JSONObject(result);
                        int code = object.optInt("code");
                        if (code == 100) {

                            Utility.showAlert(mActivity, null, "Informed Successfully");
                        } else if (code == 101) {
                            Utility.showAlert(mActivity, null, "Failed Informed");
                        } else {
                            Utility.showAlert(mActivity, null, "Unknown response");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utility.showAlert(mActivity, null, "Invalid Response contact system admin");
                }

            } else {
                Utility.showAlert(mActivity, "Network problem", "Check your internet connection");
            }
            Utility.hideProgressDialog();
        }
    };

    private String getMessageForRagging(String msg) {
        UserDTO localObject = (UserDTO) UserDAO.getInstance().getRecords(DBHandler.getInstance(this.mActivity).getDBObject(0)).get(0);
        String str = "" + localObject.getName();
        str = str + "/" + localObject.getAge();
        str = str + "" + localObject.getGender().charAt(0);
        str = str + "/MNo:" + localObject.getMobileNo();
        str = str + "\nis in danger at: " + getMapUrl(location);
        if (msg != null && !msg.isEmpty())
            str = str + "\nUSRMSG:" + msg;
        return str;
    }

    private String getMapUrl(String paramString) {
        return "https://www.google.co.in/maps/place//@" + paramString + ",16z";
    }
}
