package com.example.maheshraja.mycampus.utility;

/**
 * Created by Swamy on 2/25/2016.
 */
public class MyConstants {
    public static final String TAB_HOME = "home";
    public static final String TAB_EVENTS = "events";
    public static final String TAB_Coupons = "coupons";
    public static final String TAB_ABOUT = "about";
    public static final String TAB_SPONSERS = "sponsors";
    public static final String TAB_DASHBOARD = "dashboard";
    public static final String TAB_LOGOUT = "logout";
    public static final String TAB_HELP = "help";
    public static final String TAB_SETTINGS = "settings";

    public static final String GOT_LOCATION_MSG = "Got location";
    public static final int CONNECTION_TIMEOUT = 30 * 1000;
    public static final int SOCKET_TIMEOUT = 30 * 1000;

    public static final int IMG_CONNECTION_TIMEOUT = 60 * 1000;
    public static final int IMG_SOCKET_TIMEOUT = 60 * 1000;

    public static final int VID_CONNECTION_TIMEOUT = 2 * 60 * 1000;
    public static final int VID_SOCKET_TIMEOUT = 2 * 60 * 1000;

    public static final String CONTACT_CI = "8121222522";
    public static final String CONTACT_DCP = "8978490150";
    public static final String CONTACT_SI = "9948373220";
    public static int photoQuality = 50;

    public static final String JOBPORTAL_APP_URL = "http://192.168.1.3:8080/JobPortal/";


   public static final String APP_URL = "http://192.168.1.3:8080/MyCampus/";
    public static final String APP_URL_ISHARE = "http://192.168.1.8:8888/Ishare/";
   // public static final String APP_URL = "http://192.168.1.8:8080/MyCampus/";
    //public static final String APP_URL = "http://techgenius.cloud/Advertise/";
    public static final String VIDEO_APP_URL = "http://techgenius.cloud/FileuploadforMycampus/";

    public static final String LOGIN_URL = "rest/login/validate";
    public static final String REG_URL = "rest/usermanagement/insert";
    public static final String JobPortal_REG_URL = "rest/jobseeker/insert";
    public static final String JobPortal_Update_URL = "rest/jobseeker/edit";
    public static final String Confession_URL = "rest/Confession/insertconfession";
    public static final String JOBSEARCH_APPLY_URL = "rest/appliedjobs/insertapplied";
    public static final String JOBSEARCH_APPLYJOB_URL = "rest/appliedjobs/insertapplied";
    public static final String JOBSEARCH_ViewApplied_URL = "rest/appliedjobs/viewapplied";
    public static final String JOBPORTAL_GETJOBS_URL = "rest/search/getjobs";
    public static final String ISHARE_TYPE_URL = "rest/item/getitemsbytype";
    public static final String Confession_LIKE_URL = "rest/Confession/insertlikes";
    public static final String Confession_comment_URL = "rest/Confession/postcomment";
    public static final String UPDATE_FRIENDS_URL = "rest/friendsupdateservice/updatefriends";
    public static final String UPDATE_PROFILE_URL = "rest/userprofileupdate/update";
    public static final String DANGER_URL = "rest/dangermanagementservice/insert";
    public static final String ACCIDENT_URL = "rest/accidentmanagementservice/insert";
    public static final String RESEND_URL = "rest/resendotp/resendotptest";
    public static final String DOMESTIC_URL = "rest/domesticvoilenceservice/insert";
    public static final String RAGGING_URL = "rest/raggingmanagementservice/insert";
    public static final String SEXUAL_URL = "rest/harrassmentervice/insert";
    public static final String CHAIN_URL = "rest/chainsnatchingservice/insert";
    public static final String OTHER_URL = "rest/accidentmanagementservice/insert";
    public static final String HELP_URL = "rest/help/helpinsert";
    public static final String OTP_URL = "rest/otpverifyservice/verify";
    public static final String ADVT_URL="rest/Confession/getdetails";
    public static final String ADVT_URL1="rest/Confession/getcollages";

    public static final String JobPoral_SEARCHJOB_URL="rest/search/getkeywords";
    public static final String ADVT_URL_ISHARE="rest/itemtype";
    public static final String IMG_UPLOAD_URL = "UploadServletExample";
    public static final String Audio_UPLOAD_URL = "AudioUpload";
    public static final String VIDEO_UPLOAD_SERVICE = "VidUpload";
    public static final String AUDIO_UPLOAD_SERVICE = "AudioUpload";
    public static final String CALL_POLICE = "100";
    public static final String CALL_SURAKSHA = "1091";
    public static final String CALL_EMERGENCY = "9440627389";

    public static final String PDeniedTitle = "Permission Denied";
    public static final String PDeniedMessage = "Allow all the permissions to use the services\nif you denia any one permission application will not allow you to use wervices";
    public static final String PNDeniedTitle = "Permission Denied";
    public static final String PNDeniedMessage = "You denied permission(s) with never show option\nPlease enable permissions manually goto app info By clicking on settings button and tab on permissions then enable all the permissions";

}
