package com.example.maheshraja.mycampus.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.example.maheshraja.mycampus.asynctask.VideoAsyncTask;
import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;

public class AndroidVideoCapture extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static final String TAG = "VideoActivity";
    private Uri fileUri;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;
    public static AndroidVideoCapture ActivityContext =null;


    protected static GoogleApiClient mGoogleApiClient;
    protected static LocationRequest mLocationRequest;
    private CountDownTimer mLocationCountDownTimer;
    protected final int COUNT_DOWN_TIME = 30000;
    protected final int COUNT_DOWN_TIME_INTERVAL = 1000;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.main);
        ActivityContext = this;
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        // create a file to save the video
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);

        // set the image file name
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // set the video image quality to high
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
        intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, 5 * 1048 * 1048);//5*1048*1048=5MB
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 120);

        // start the Video Capture Intent
        startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
    }

     /*   Button buttonRecording = (Button)findViewById(R.id.recording);

        buttonRecording.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View arg0) {

                // create new Intentwith with Standard Intent action that can be
                // sent to have the camera application capture an video and return it.


            }});

    }*/

    /** Create a file Uri for saving an image or video */
    private static Uri getOutputMediaFileUri(int type){

        return Uri.fromFile(getOutputMediaFile(type));
    }


    static String  fileName;
    private Context context = null;
    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(int type){

        // Check that the SDCard is mounted
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MySafeVideo");

        // Create the storage directory(MyCameraVideo) if it does not exist
        if (! mediaStorageDir.exists()){

            if (! mediaStorageDir.mkdirs()){

                Toast.makeText(ActivityContext, "Failed to create directory MyCameraVideo.",
                        Toast.LENGTH_LONG).show();

                Log.d("MyCameraVideo", "Failed to create directory MyCameraVideo.");
                return null;
            }
        }


        // Create a media file name

        // For unique file name appending current timeStamp with file name
        java.util.Date date= new java.util.Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(date.getTime());

        File mediaFile;
        fileName = mediaStorageDir.getPath() + File.separator +
                "VID_"+ timeStamp + ".mp4";
        if(type == MEDIA_TYPE_VIDEO) {
            //UserDAO.getInstance().getRecords(DBHandler.getInstance(AndroidVideoCapture.this).getDBObject(0));
            // For unique video file name appending current timeStamp with file name
            mediaFile = new File(fileName);

        } else {
            return null;
        }

        return mediaFile;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // After camera screen this code will excuted
        context=this;
        if (requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {

                // Video captured and saved to fileUri specified in the Intent
                Toast.makeText(this, "Video saved to: " +
                        data.getData(), Toast.LENGTH_LONG).show();

                Utility.showAlert(context, null, "Video saved to: " +
                        data.getData());
                context= this;
                VideoAsyncTask imageAsyncTask = new VideoAsyncTask(context,data.getData().toString(), taskCompleteListener, MyConstants.IMG_CONNECTION_TIMEOUT, MyConstants.IMG_SOCKET_TIMEOUT);
                Utility.showProgressDialog(context);
                imageAsyncTask.execute(MyConstants.VIDEO_UPLOAD_SERVICE);

            } else if (resultCode == RESULT_CANCELED) {

                // User cancelled the video capture
                Toast.makeText(this, "User cancelled the video capture.",
                        Toast.LENGTH_LONG).show();

            } else {

                // Video capture failed, advise user
                Toast.makeText(this, "Video capture failed.",
                        Toast.LENGTH_LONG).show();
            }

            System.out.println("Hello");
        }
    }

    private AsyncTaskCompleteListener taskCompleteListener = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result) {
            if (result != null) {
                if(Utility.isJSONValid(result)){
                    try {
                        JSONObject object = new JSONObject(result);
                        int code = object.optInt("code");
                        if(code == 100){
                            Utility.showToast(context, "Uploaded Your Video Successfully");
                        }else if(code == 101){
                            Utility.showToast(context, "Failed Upload Video");
                        }else{
                            Utility.showToast(context, "Unknown response");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Utility.showToast(context, "Invalid Response contact system admin");
                }

            } else {
                Utility.showToast(context, "Check your internet connection");
            }
            Utility.hideProgressDialog();
            finish();
        }
    };

    /**
     * Creating google api client object
     * */
    protected synchronized void buildGoogleApiClient() {
        if(mGoogleApiClient == null){
            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API).build();
            createLocationRequest();
        }
    }

    /**
     * Creating location request object
     * */
    protected void createLocationRequest() {
        if(mLocationRequest == null){
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(0);
            mLocationRequest.setFastestInterval(0);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setSmallestDisplacement(0); // 10 meters
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());

    }

    @Override
    public void onConnected(Bundle arg0) {
        if(location != null && location.length() > 0)
            return;
        try {
            Location loc = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (loc != null) {
                location = "" + loc.getLatitude() + "," + loc.getLongitude();
            }
        }catch (SecurityException e){
            e.printStackTrace();
        }

        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if(lm.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            startLocationUpdates();
            mLocationCountDownTimer = new CountDownTimer(COUNT_DOWN_TIME, COUNT_DOWN_TIME_INTERVAL) {

                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    try {
                        stopLocationUpdates();
                        Location loc = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        if (loc != null) {
                            location = "" + loc.getLatitude() + "," + loc.getLongitude();
                        }
                    }catch (SecurityException e){

                    }
                }
            };
            mLocationCountDownTimer.start();
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();

    }

    /**
     * Starting the location updates
     * */
    protected void startLocationUpdates() {
        try {
            if (mGoogleApiClient.isConnected())
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }catch (SecurityException e){

        }

    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
        if(mGoogleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onLocationChanged(Location loc) {
        // Assign the new location
        //mLastLocation = location;

        if(loc != null){
            location = ""+loc.getLatitude()+","+loc.getLongitude();
            stopLocationUpdates();
            cancelLocationTimer();
        }

    }

    private void cancelLocationTimer(){
        if(mLocationCountDownTimer != null){
            mLocationCountDownTimer.cancel();
            mLocationCountDownTimer = null;
        }
    }

    public void onStart() {
        super.onStart();
        if ((mGoogleApiClient != null) && (!mGoogleApiClient.isConnected())) {
            mGoogleApiClient.connect();
        }

        // ---when the SMS has been sent---
//        mActivity.registerReceiver(sendBroadcast, new IntentFilter(SENT));

        // ---when the SMS has been delivered---
//        mActivity.registerReceiver(deliverBroadcast, new IntentFilter(DELIVERED));
    }

    public void onStop() {
        super.onStop();
        if ((mGoogleApiClient != null) && (mGoogleApiClient.isConnected())) {
            mGoogleApiClient.disconnect();
        }

//        mActivity.unregisterReceiver(sendBroadcast);
//        mActivity.unregisterReceiver(deliverBroadcast);
    }
}
//}
