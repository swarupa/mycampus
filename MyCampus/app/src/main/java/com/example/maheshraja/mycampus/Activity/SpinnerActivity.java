package com.example.maheshraja.mycampus.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.adapter.ListAdapter;
import com.example.maheshraja.mycampus.asynctask.SpinnerAsyncTask;
import com.example.maheshraja.mycampus.dtos.ConfessionDTO;
import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SpinnerActivity extends AppCompatActivity {
    private Context context;
    List<ConfessionDTO> list = new ArrayList<>();
    String name=null;
    //List<String> list= new ArrayList<String>();
    ListAdapter adapter;
    ConfessionDTO st;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);
        Spinner spiner = (Spinner) findViewById(R.id.spinner);

        List<String> lables = new ArrayList<String>();
        System.out.println("lables" + lables);

//        lables.add(list.get(list.size()).getType());

        for (int i = 0; i < list.size(); i++) {
            lables.add(list.get(i).getCollege_name());
            // System.out.println(lables.add(list.get(list.size()).getType()));
        }

        // Creating adapter for spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, lables);


        spiner.setAdapter(spinnerAdapter);


        getSponsers();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void getSponsers() {

        SpinnerAsyncTask sponser = new SpinnerAsyncTask(completeListener, MyConstants.CONNECTION_TIMEOUT, MyConstants.SOCKET_TIMEOUT);
        Utility.showProgressDialog(context);
        sponser.execute(MyConstants.ADVT_URL);
    }

    private AsyncTaskCompleteListener completeListener = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result) {
            if (result != null && !result.isEmpty()) {
                if (Utility.isJSONValid(result)) {

                    try {


                        JSONArray ar = new JSONArray(result);
                        if (ar != null && ar.length() > 0) {
                            for (int i = 0; i < ar.length(); i++) {
                                JSONObject obj = ar.getJSONObject(i);
                                ConfessionDTO dto = new ConfessionDTO();
                              dto.setCollege_name(obj.getString("college_name"));
                                System.out.println("ar=" + ar);


                                list.add(dto);

                            }


                        }
                        if (list.size() > 0) {
                            //populateData();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else {
                    //Utility.showAlert(LoginActivity.this, null, "Invalid Response contact system admin");
                    //Utility.showAlert(SponsorsFragment.this, null, "Invali userId/password");
                }

            } else {
                //Utility.showAlert(LoginActivity.this, "Network problem", "Check your internet connection");
                // Utility.showAlert(LoginActivity.this, null, "Invali userId/password");
            }
            Utility.hideProgressDialog();
        }
    };


}


