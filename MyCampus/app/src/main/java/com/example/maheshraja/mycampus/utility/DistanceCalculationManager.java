package com.example.maheshraja.mycampus.utility;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

public class DistanceCalculationManager {
    public static void main(String[] args) throws Exception {

        //new DistanceCalculationManager().getNearestSHO(15.4334524, 80.0437099); // RISE COLLEGE																				// COLLEGE
        //new DistanceCalculationManager().getNearestSHO(15.827450, 80.355868); // CHIRALA ONE TOWN
    }

    static Map<String, String> shoMap = new HashMap();

    static {
        shoMap.put("Chinaganjam", "15.695295, 80.240736");
        shoMap.put("Chirala I town", "15.827445, 80.355868");
        shoMap.put("Chirala rural", "15.854701, 80.393529");
        shoMap.put("Chirala II town", "15.827914, 80.374890");
        shoMap.put("Inkollu", "15.826359, 80.193741");
        shoMap.put("J. Pangulur", "15.805177, 80.040434");
        shoMap.put("Parchur", "15.969026, 80.274546");
        shoMap.put("Yaddanapudi", "15.997342, 80.172461");
        shoMap.put("Gudlur", "15.073367, 79.901703");
        shoMap.put("Kandukur town", "15.217313, 79.907934");
        shoMap.put("Kandukur rural", "15.217901, 79.898799");
        shoMap.put("Kanigiri", "15.403715, 79.505074");
        shoMap.put("Pamur", "15.094419, 79.410324");
        shoMap.put("Ponnalur", "15.272222, 79.799135");
        shoMap.put("Ulavapadu", "15.170211, 80.010586");
        shoMap.put("Valetivaripalem", "15.166583, 79.728387");
        shoMap.put("Veligandla", "15.341303, 79.345219");
        shoMap.put("Gudlur", "15.073367, 79.901703");
        shoMap.put("Kandukur town", "15.217313, 79.907934");
        shoMap.put("Kandukur rural", "15.217901, 79.898799");
        shoMap.put("Kanigiri", "15.403715, 79.505074");
        shoMap.put("Pamur", "15.094419, 79.410324");
        shoMap.put("Ponnalur", "15.272222, 79.799135");
        shoMap.put("Ulavapadu", "15.170211, 80.010586");
        shoMap.put("Valetivaripalem", "15.166583, 79.728387");
        shoMap.put("Veligandla", "15.341303, 79.345219");
        shoMap.put("Ardhaveedu", "15.634375, 79.040404");
        shoMap.put("B V Peta", "15.522149, 79.105355");
        shoMap.put("Cumbum", "15.589232, 79.115785");
        shoMap.put("Dornala", "15.898872, 79.103261");
        shoMap.put("Giddaluru", "15.377136, 78.923272");
        shoMap.put("Komarole", "15.269584, 78.996679");
        shoMap.put("Markapur rural", "15.738929, 79.270366");
        shoMap.put("Markapur town", "15.766295, 79.279450");
        shoMap.put("Pullalacheruvu", "16.039361, 79.303996");
        shoMap.put("Peddaraveedu", "15.745690, 79.248051");
        shoMap.put("Racherla", "15.455507, 78.962694");
        shoMap.put("Tripuranthakam", "15.998559, 79.454172");
        shoMap.put("Yerragondapalem", "16.039825, 79.306140");
        shoMap.put("Chimakurthy", "25.0000,85.000");
        shoMap.put("Jarugumalli", "15.314545, 79.989132");
        shoMap.put("Kondapi", "15.413661, 79.854750");
        shoMap.put("Kothapatnam", "15.445013, 80.159790");
        shoMap.put("Maddipadu", "15.622222, 80.021383");
        shoMap.put("N.G. Padu", "15.641806, 80.112354");
        shoMap.put("Ongole I town", "12.500102, 80.047990");
        shoMap.put("Ongole taluka", "15.505723, 80.049922");
        shoMap.put("Ongole II town", "15.502354, 80.053304");
        shoMap.put("Santhanuthalapadu", "15.545936, 79.939237");
        shoMap.put("Singarayakonda", "15.252406, 80.022926");
        shoMap.put("Tangutur", "15.341644, 80.037599");
        shoMap.put("Ballikurava", "15.995111, 80.024852");
        shoMap.put("Donakonda", "15.835584, 79.483387");
        shoMap.put("Korisapadu", "15.756382, 80.036137");
        shoMap.put("Kurichedu", "15.903617, 79.577901");
        shoMap.put("Marripudi", "15.514154, 79.652607");
        shoMap.put("Medarametla", "15.725983, 80.019572");
        shoMap.put("Podili", "15.606342, 79.610352");
        shoMap.put("Thallur", "15.738758, 79.881842");
    }

    static Map<String, String> shoCell = new HashMap();

    static{
        shoCell.put("Chinaganjam","9440627111");
        shoCell.put("Chirala I town","9440627139");
        shoCell.put("Chirala rural","9948373220");
        shoCell.put("Chirala II town","9440627140");
        shoCell.put("Inkollu","9440627174");
        shoCell.put("J. Pangulur","9440627146");
        shoCell.put("Parchur","9440627144");
        shoCell.put("Yaddanapudi","9440627147");
        shoCell.put("Ballikurava","9440627159");
        shoCell.put("Donakonda","9440627116");
        shoCell.put("Korisapadu","9440627843");
        shoCell.put("Kurichedu","9440627842");
        shoCell.put("Marripudi","9440627115");
        shoCell.put("Medarametla","9440627158");
        shoCell.put("Podili","9440627113");
        shoCell.put("Thallur","9440627386");
        shoCell.put("Gudlur","9440627835");
        shoCell.put("Kandukur town","9440627150");
        shoCell.put("Kandukur rural","9440627151");
        shoCell.put("Kanigiri","9440627153");
        shoCell.put("Pamur","9440627179");
        shoCell.put("Ponnalur","9440627828");
        shoCell.put("Ulavapadu","9440627836");
        shoCell.put("Valetivaripalem","9440627834");
        shoCell.put("Veligandla","9440627829");
        shoCell.put("Ardhaveedu","9440627108");
        shoCell.put("B V Peta","9440627384");
        shoCell.put("Cumbum","9440627109");
        shoCell.put("Dornala","9440627840");
        shoCell.put("Giddaluru","9440627383");
        shoCell.put("Komarole","9440627385");
        shoCell.put("Markapur rural","9440627106");
        shoCell.put("Markapur town","9440627105");
        shoCell.put("Pullalacheruvu","9440627189");
        shoCell.put("Peddaraveedu","9440627107");
        shoCell.put("Racherla","9440627839");
        shoCell.put("Tripuranthakam","9440627190");
        shoCell.put("Yerragondapalem","9440627188");
        shoCell.put("Chimakurthy","9440627125");
        shoCell.put("Jarugumalli","9440627132");
        shoCell.put("Kondapi","9440627133");
        shoCell.put("Kothapatnam","9440627123");
        shoCell.put("Maddipadu","9440627127");
        shoCell.put("N.G. Padu","9440627128");
        shoCell.put("Ongole I town ","9440627119");
        shoCell.put("Ongole taluka","9440627122");
        shoCell.put("Ongole II town","9440627121");
        shoCell.put("Santhanuthalapadu","9440627126");
        shoCell.put("Singarayakonda","9440627130");
        shoCell.put("Tangutur","9440627131");

    }

    public String getSHOCellNumber(String shoName){

        return shoCell.get(shoName);
    }

    public String getNearestSHO(double lat1, double lon1) {

        Iterator<?> entries = shoMap.entrySet().iterator();

        Map<Object, Object> distanceMap = new HashMap<>();

        double[] distancesArray = new double[shoMap.size()];
        int i = 0;
        while (entries.hasNext()) {
            Map.Entry<?, ?> thisEntry = (Map.Entry<?, ?>) entries.next();
            Object key = thisEntry.getKey();
            String value = (String) thisEntry.getValue();

            StringTokenizer st2 = new StringTokenizer(value + "", ",");

            String lattitute = "";
            String longitute = "";

            while (st2.hasMoreElements()) {
                lattitute = (String) st2.nextElement();
                longitute = st2.nextToken();
                double distance = getDistance(lat1, lon1, Double.parseDouble(lattitute), Double.parseDouble(longitute),
                        "K");
                // System.out.println("Checking for Lat :"+lattitute +" and Long
                // :"+longitute);
                System.out.println(key + "[" + lattitute + "," + longitute + " ] -> Distance =" + distance);
                distanceMap.put(distance, key);
                distancesArray[i] = distance;
                System.out.println("Distance[" + i + "]=" + distancesArray[i]);
                i++;
            }
        }
        System.out.println("\n---------------------------------------\n"+distanceMap);

        Arrays.sort(distancesArray);

        System.out.println("Nearest Police Station  Identified is " + distanceMap.get(distancesArray[0]) + "Distance ="
                + distancesArray[0]);
        return "" + distanceMap.get(distancesArray[0]);
    }

	/*public String getSHOCellNum(String shoName){
		return "9948373220";
	}*/

    public double getDistance(double lat1, double lon1, double lat2, double lon2, String unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == "K") {
            dist = dist * 1.609344;
        } else if (unit == "N") {
            dist = dist * 0.8684;
        }

        return (dist);

    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }
}