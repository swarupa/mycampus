package com.example.maheshraja.mycampus.asynctask;

/**
 * Created by Swamy on 3/4/2016.
 */

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.util.Log;

import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.Utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * Created by Yakaswamy.g on 2/22/2016.
 */

public class DownloadImageAsync extends AsyncTask<String, Void, String> {

    private AsyncTaskCompleteListener asyncTaskCompleteListener;
    private int connectionTimeout;
    private int socketTimeout;
    private Context context;

    public DownloadImageAsync(Context context, AsyncTaskCompleteListener asyncTaskCompleteListener, int connectionTimeout, int socketTimeout) {
        this.asyncTaskCompleteListener = asyncTaskCompleteListener;
        this.connectionTimeout = connectionTimeout;
        this.socketTimeout = socketTimeout;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        String filepath = null;
        try
        {
            URL url = new URL(params[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setConnectTimeout(connectionTimeout);
            urlConnection.setReadTimeout(socketTimeout);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            File dir;
            if (Utility.getFileDir(context) != null && Utility.getFileDir(context).length() > 0) {
                dir = new File(Utility.getFileDir(context));
            } else {

                PackageManager m = context.getPackageManager();
                String s = context.getPackageName();
                try {
                    PackageInfo p = m.getPackageInfo(s, 0);
                    s = p.applicationInfo.dataDir;
                } catch (PackageManager.NameNotFoundException e) {
                    Log.w("yourtag", "Error Package name not found ", e);
                }

                File sdCard = new File(s);
                dir = new File(sdCard.getAbsolutePath() + "/iSave");

                if (!dir.exists()) {
                    dir.mkdir();
                }

                Utility.setFileDir(dir.toString(), context);
            } // boolean
            //status = isExternalStoragePresent();

            String uniqueId = Utility.getCurrentDate().replaceAll("/", "_") + "_" + System.currentTimeMillis() + ".png";

            File mypath = new File(dir, uniqueId);

            FileOutputStream fileOutput = new FileOutputStream(mypath);
            InputStream inputStream = urlConnection.getInputStream();
            int totalSize = urlConnection.getContentLength();
            int downloadedSize = 0;
            byte[] buffer = new byte[1024];
            int bufferLength = 0;
            while ( (bufferLength = inputStream.read(buffer)) > 0 )
            {
                fileOutput.write(buffer, 0, bufferLength);
                downloadedSize += bufferLength;
                Log.i("Progress:","downloadedSize:"+downloadedSize+"totalSize:"+ totalSize) ;
            }
            fileOutput.close();
            if(downloadedSize==totalSize) filepath=mypath.getPath();
            Utility.setProfileImageURL(uniqueId, context);
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            filepath=null;
            e.printStackTrace();
        }
        return filepath;
    }

    @Override
    protected void onPostExecute(String result) {
        if(asyncTaskCompleteListener != null)
            asyncTaskCompleteListener.onAsynComplete(result);
    }
}


