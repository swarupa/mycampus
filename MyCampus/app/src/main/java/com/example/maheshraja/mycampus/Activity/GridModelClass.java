package com.example.maheshraja.mycampus.Activity;

import java.io.Serializable;

/**
 * Created by bhuvaneswari on 06-07-2016.
 */
public class GridModelClass implements Serializable {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
