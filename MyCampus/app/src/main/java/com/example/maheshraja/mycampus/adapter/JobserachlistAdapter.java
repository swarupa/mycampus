package com.example.maheshraja.mycampus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.dtos.JobPortalDTO;

import java.util.List;

/**
 * Created by lenovo on 29-07-2016.
 */
public class JobserachlistAdapter extends BaseAdapter {
    JobPortalDTO dto;
    private List<JobPortalDTO> list;
    private Context context;

    public JobserachlistAdapter(List<JobPortalDTO> list, Context context) {
        this.list = list;
        this.context = context;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View view = LayoutInflater.from(context).inflate(R.layout.jobsearch_xml, null);

        dto = list.get(position);
        System.out.println(dto);

        final TextView text = (TextView) view.findViewById(R.id.subtextview);
        final TextView text1 = (TextView) view.findViewById(R.id.subtextview1);
        final TextView text2 = (TextView) view.findViewById(R.id.subtextview2);
        final TextView text3 = (TextView) view.findViewById(R.id.subtextview3);
        final TextView text4 = (TextView) view.findViewById(R.id.subtextview4);
        final TextView text5 = (TextView) view.findViewById(R.id.subtextview5);

        text.setText(dto.getTagline());
        text1.setText(dto.getCompany_name());
        text2.setText(dto.getCompany_loc());
        text3.setText(dto.getKeyword());
        text4.setText(dto.getWanted_exp());

        System.out.println(dto.getCompany_name());
        return view;
    }
}
