package com.example.maheshraja.mycampus.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;

import java.util.ArrayList;
import java.util.List;

public class PermissionsActivity extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_FIRST = 100;
    private static final int MY_PERMISSIONS_REQUEST_REPEAT = 101;
    private static final int MY_PERMISSIONS_REQUEST_DENIAL = 102;
    private final String appPackageName="com.empover.vjy.vpcs";
    private Activity thisActivity;
    List<String> permissionsList = new ArrayList<String>();
    List<String> reqPermisList = new ArrayList<String>();
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissions);
        thisActivity = this;
    }
    
    {
    	reqPermisList.add(Manifest.permission.READ_CONTACTS);
    	reqPermisList.add(Manifest.permission.ACCESS_FINE_LOCATION);
    	reqPermisList.add(Manifest.permission.CAMERA);
    	reqPermisList.add(Manifest.permission.CALL_PHONE);
    	reqPermisList.add(Manifest.permission.RECEIVE_SMS);
    	reqPermisList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        reqPermisList.add(Manifest.permission.RECORD_AUDIO);
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    	final int apiLevel = Build.VERSION.SDK_INT;
        if (apiLevel >= 23) {
        	checkPermissions();
        }else{
        	navigateToMain();
        }
    }

    private boolean isPermissionGrant(final String PERMISSION){
    	
        if(ContextCompat.checkSelfPermission(thisActivity, PERMISSION) == PackageManager.PERMISSION_GRANTED)
            return true;
        return false;
    }

    private boolean permissionRotational(String permission){
        if(ActivityCompat.shouldShowRequestPermissionRationale(thisActivity, permission))
            return true;
        return false;
    }

    private boolean isPermissionDenied(String permission){
        return false;
    }

    private void navigateToMain(){
    	Intent intent = new Intent(PermissionsActivity.this, SplashScreenActivity.class);
    	startActivity(intent);
    	finish();
    }

    private boolean theseRotational(List<String> list){
        if(list != null && list.size() > 0){
            for(String permission : list){
                if(permissionRotational(permission))
                    return true;
            }
        }

        return false;
    }

    private boolean theseNotRotational(List<String> list){
        if(list != null && list.size() > 0){
            for(String permission : list){
                if(!permissionRotational(permission))
                    return true;
            }
        }

        return false;
    }

    private void checkPermissions(){
        permissionsList.clear();
        for(String permission : reqPermisList){
        	if(!isPermissionGrant(permission))
                permissionsList.add(permission);
        }
        
        if(permissionsList.size() == 0) {
            navigateToMain();
            return;
        }

        String[] permissionsArray = new String[permissionsList.size()];
        for(int i = 0; i < permissionsList.size(); i++){
            permissionsArray[i] = permissionsList.get(i);
        }

        if(permissionsList.size() == reqPermisList.size()){
            if(!theseNotRotational(permissionsList)){
                ActivityCompat.requestPermissions(thisActivity, permissionsArray,
                        MY_PERMISSIONS_REQUEST_REPEAT);
            }else if(theseNotRotational(permissionsList)){
                    if(Utility.isFirst(thisActivity)){
                        Utility.setFirst(false, thisActivity);
                        ActivityCompat.requestPermissions(thisActivity, permissionsArray, MY_PERMISSIONS_REQUEST_FIRST);
                    }else{
                        ActivityCompat.requestPermissions(thisActivity, permissionsArray, MY_PERMISSIONS_REQUEST_DENIAL);
                    }
                }
            }else if(theseRotational(permissionsList)) {
                ActivityCompat.requestPermissions(thisActivity, permissionsArray, MY_PERMISSIONS_REQUEST_REPEAT);
            }else{
            	ActivityCompat.requestPermissions(thisActivity, permissionsArray, MY_PERMISSIONS_REQUEST_DENIAL);
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_FIRST: {
                if(grantResults.length > 0){
                    int len = grantResults.length;
                    int count = 0;
                    for(int i = 0; i < grantResults.length; i++){
                        // If request is cancelled, the result arrays are empty.
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            count++;
                            // permission was granted, yay! Do the
                            // contacts-related task you need to do.
                        } else {
                            // permission denied, boo! Disable the
                            // functionality that depends on this permission.
                        }
                    }
                    if(len != count){
                        showDialog();
                    }else{
                    	navigateToMain();
                    }
                }

                return;
            }

            case MY_PERMISSIONS_REQUEST_REPEAT:{
                if(grantResults.length > 0){
                    int len = grantResults.length;
                    int count = 0;
                    for(int i = 0; i < grantResults.length; i++){
                        // If request is cancelled, the result arrays are empty.
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            count++;
                            // permission was granted, yay! Do the
                            // contacts-related task you need to do.
                        } else {
                            // permission denied, boo! Disable the
                            // functionality that depends on this permission.
                        }
                    }
                    if(len != count){
                        showDialog();
                    }else{
                    	navigateToMain();
                    }
                }

                return;
            }

            case MY_PERMISSIONS_REQUEST_DENIAL: {
                if (grantResults.length > 0) {
                    int len = grantResults.length;
                    int count = 0;
                    for (int i = 0; i < grantResults.length; i++) {
                        // If request is cancelled, the result arrays are empty.
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            count++;
                            // permission was granted, yay! Do the
                            // contacts-related task you need to do.
                        } else {
                            // permission denied, boo! Disable the
                            // functionality that depends on this permission.
                        }
                    }
                    if(len != count){
                    	showDialogToOpenSetting();
                    }else{
                    	navigateToMain();
                    }
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void showDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(thisActivity);
        builder.setTitle(MyConstants.PDeniedTitle);
        builder.setMessage(MyConstants.PDeniedMessage);
        builder.setPositiveButton("RE-TRY", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                checkPermissions();
            }
        });

        builder.setNegativeButton("I'M SURE", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.create().show();
    }

    private void showDialogToOpenSetting(){
        AlertDialog.Builder builder = new AlertDialog.Builder(thisActivity);
        builder.setTitle(MyConstants.PNDeniedTitle);
        builder.setMessage(MyConstants.PNDeniedMessage);
        builder.setPositiveButton("Open Settings", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                showInstalledAppDetails(thisActivity, thisActivity.getPackageName());
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.create().show();
    }

    public void showInstalledAppDetails(Context context, String packageName) {
    	
    	Intent intent2 = new Intent();
    	intent2.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
    	Uri uri2 = Uri.fromParts("package", thisActivity.getPackageName(), null);
    	intent2.setData(uri2);
    	context.startActivity(intent2);
    	finish();
    	

        /*final String SCHEME = "package";

        final String APP_PKG_NAME_21 = "com.android.settings.ApplicationPkgName";

        final String APP_PKG_NAME_22 = "pkg";

        final String APP_DETAILS_PACKAGE_NAME = "com.android.settings";

        final String APP_DETAILS_CLASS_NAME = "com.android.settings.InstalledAppDetails";


        Intent intent = new Intent();
        final int apiLevel = Build.VERSION.SDK_INT;
        if (apiLevel >= 9) { // above 2.3
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts(SCHEME, packageName, null);
            intent.setData(uri);
        } else { // below 2.3
            final String appPkgName = (apiLevel == 8 ? APP_PKG_NAME_22
                    : APP_PKG_NAME_21);
            intent.setAction(Intent.ACTION_VIEW);
            intent.setClassName(APP_DETAILS_PACKAGE_NAME,
                    APP_DETAILS_CLASS_NAME);
            intent.putExtra(appPkgName, packageName);
        }
        context.startActivity(intent);
        finish();*/
    }

}
