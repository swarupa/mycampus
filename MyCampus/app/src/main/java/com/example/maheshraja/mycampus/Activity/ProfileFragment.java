package com.example.maheshraja.mycampus.Activity;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.daos.JobportalDAo;
import com.example.maheshraja.mycampus.database.DBHandler;
import com.example.maheshraja.mycampus.dtos.JobPortalDTO;
import com.example.maheshraja.mycampus.utility.MyConstants;


public class ProfileFragment extends BaseFragment {

    private TextView txtName, txtMobile, txtEmail, txtpreffredloc, txtresum, txtssc, txtinter,txtbtech,txtkeyskills;

    private Button btnEdit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        txtName = (TextView) rootView.findViewById(R.id.pro_name);
        txtMobile = (TextView) rootView.findViewById(R.id.pro_mobile);
        txtEmail = (TextView) rootView.findViewById(R.id.pro_email);
        txtkeyskills = (TextView) rootView.findViewById(R.id.pro_keyskill);
        txtresum = (TextView) rootView.findViewById(R.id.pro_resume);
        txtssc = (TextView) rootView.findViewById(R.id.pro_sscper);
        txtinter = (TextView) rootView.findViewById(R.id.pro_inter);
        txtbtech = (TextView) rootView.findViewById(R.id.pro_btech);
        txtpreffredloc = (TextView) rootView.findViewById(R.id.pro_loc);
        btnEdit = (Button) rootView.findViewById(R.id.pro_edit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.pushFragments(MyConstants.TAB_HOME, new EditProfileFragment(), false, true);
            }
        });

        JobPortalDTO dto = (JobPortalDTO) JobportalDAo.getInstance().getRecords(DBHandler.getInstance(this.mActivity).getDBObject(0)).get(0);
        System.out.println("Retrive from local DataBase");
        txtName.setText(dto.getUserName());
        System.out.println(dto.getUserName());
        txtMobile.setText(dto.getMobileNumber());
        System.out.println(dto.getMobileNumber());
        txtEmail.setText(dto.getEmailAddress());
        System.out.println(dto.getEmailAddress());
        txtkeyskills.setText(dto.getKeySkills());
        System.out.println(dto.getKeySkills());
        txtresum.setText(dto.getResumeHeadline());
        System.out.println(dto.getResumeHeadline());
        txtssc.setText(dto.getSSCPercentage());
        System.out.println(dto.getSSCPercentage());
        txtinter.setText(dto.getInterPercentage());
        System.out.println(dto.getInterPercentage());
        txtbtech.setText(dto.getBtechPercentage());
        System.out.println(dto.getBtechPercentage());
        txtpreffredloc.setText(dto.getPreferedLocation());
        System.out.println(dto.getPreferedLocation());





        return rootView;
    }


}
