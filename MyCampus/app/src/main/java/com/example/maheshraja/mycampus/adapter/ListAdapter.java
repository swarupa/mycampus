package com.example.maheshraja.mycampus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Spinner;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.dtos.ConfessionDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 15-07-2016.
 */
public class ListAdapter extends BaseAdapter {

    private List<ConfessionDTO> list;

    Context context;

    public ListAdapter(List<ConfessionDTO> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.fragment_condenssion, null);

        Spinner type = (Spinner) view.findViewById(R.id.list);
        List<String> lables = new ArrayList<String>();
        System.out.println("lables" + lables);

//        lables.add(list.get(list.size()).getType());

        for (int i = 0; i < list.size(); i++) {
            lables.add(list.get(i).getCollege_name());
           // System.out.println(lables.add(list.get(list.size()).getType()));
        }

        // Creating adapter for spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, lables);


        type.setAdapter(spinnerAdapter);






        return view;
    }
}