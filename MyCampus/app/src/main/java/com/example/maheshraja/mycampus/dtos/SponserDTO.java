package com.example.maheshraja.mycampus.dtos;

/**
 * Created by MaheshRaja on 6/28/2016.
 */
public class SponserDTO {

    String sponserName;
    String sponserId;
    String logopageUrl;
    String loginpageUrl;

    public String getSponserName() {
        return sponserName;
    }

    public void setSponserName(String sponserName) {
        this.sponserName = sponserName;
    }

    public String getSponserId() {
        return sponserId;
    }

    public void setSponserId(String sponserId) {
        this.sponserId = sponserId;
    }

    public String getLogopageUrl() {
        return logopageUrl;
    }

    public void setLogopageUrl(String logopageUrl) {
        this.logopageUrl = logopageUrl;
    }

    public String getLoginpageUrl() {
        return loginpageUrl;
    }

    public void setLoginpageUrl(String loginpageUrl) {
        this.loginpageUrl = loginpageUrl;
    }


    public  String toString(){
        StringBuffer bf=new StringBuffer();
        bf.append("sponserName="+sponserName);
        bf.append("sponserId"+sponserId);
        bf.append("logopageUrl"+logopageUrl);
        bf.append("loginpageUrl"+loginpageUrl);


        return bf.toString();
    }

}
