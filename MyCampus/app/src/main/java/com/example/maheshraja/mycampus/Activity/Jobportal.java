package com.example.maheshraja.mycampus.Activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.asynctask.SearchJobAsynTask;
import com.example.maheshraja.mycampus.daos.JobportalDAo;
import com.example.maheshraja.mycampus.database.DBHandler;
import com.example.maheshraja.mycampus.dtos.JobPortalDTO;
import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 20-07-2016.
 */

public class Jobportal  extends  BaseFragment{

    TextView text,text1,text3,textusername;
    List<JobPortalDTO> list = new ArrayList<>();
    List<String> lables = new ArrayList<>();
    List<JobPortalDTO> list1 = new ArrayList<>();
    ArrayList<String> lables1= new ArrayList<>();
    private Context context;
    JobPortalDTO st;
    JSONObject obj;
    TextView job;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.jobportal, container, false);
context=mActivity;

        textusername = (TextView) rootView.findViewById(R.id.username);

        JobPortalDTO user = (JobPortalDTO) JobportalDAo.getInstance().getRecords(DBHandler.getInstance(context).getDBObject(0)).get(0);

        textusername.setText("WELCOME ! " + user.getUserName().toUpperCase());

        text = (TextView) rootView.findViewById(R.id.button1);
        job = (TextView) rootView.findViewById(R.id.button1);
        text1 = (TextView) rootView.findViewById(R.id.button2);
        text3 = (TextView) rootView.findViewById(R.id.button3);
       /* *//*Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.jobsearch);
        text.setImageBitmap(getCircleBitmap(bm));*//*

        Bitmap bm1 = BitmapFactory.decodeResource(getResources(), R.drawable.applyjob);
        text1.setImageBitmap(getCircleBitmap(bm1));


        Bitmap bm2 = BitmapFactory.decodeResource(getResources(), R.drawable.profileupdate);
   text3.setImageBitmap(getCircleBitmap(bm2));*/

        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.button1:
                        mActivity.pushFragments(MyConstants.TAB_HOME, new Searchforjob(), false, true);
                        break;

                }

            }
        });


        text1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getId()==R.id.button2)
                   sendData();

            }
        });

        text3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.pushFragments(MyConstants.TAB_HOME, new ProfileFragment(), false, true);
            }
        });
        return rootView;
    }






    private void sendData() {


            JobPortalDTO user = (JobPortalDTO) JobportalDAo.getInstance().getRecords(DBHandler.getInstance(context).getDBObject(0)).get(0);
        JSONObject reqJson = new JSONObject();

        try {

            reqJson.put("username", user.getEmailAddress());
            System.out.println("username" + user.getEmailAddress());

        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){

        }

        SearchJobAsynTask loginAsync = new SearchJobAsynTask(reqJson.toString(), taskCompleteListener, MyConstants.CONNECTION_TIMEOUT, MyConstants.SOCKET_TIMEOUT);
        Utility.showProgressDialog(context);
        loginAsync.execute(MyConstants.JOBSEARCH_ViewApplied_URL);

    }

    private AsyncTaskCompleteListener taskCompleteListener = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result1) {
            if (result1 != null && !result1.isEmpty()) {
                if (Utility.isJSONValid(result1)) {

                    System.out.println("test AsyncTask for retrive");
                    try {


                        JSONArray ar = new JSONArray(result1);
                        if (ar != null && ar.length() > 0) {
                            for (int i = 0; i < ar.length(); i++) {
                                obj= ar.getJSONObject(i);
                                JobPortalDTO dto = new JobPortalDTO();
                                dto.setCompany_name(obj.getString("company_name"));
                                dto.setCompany_loc(obj.getString("company_location"));
                                dto.setInterview_loc(obj.getString("interview_loc"));


                                dto.setKeyword(obj.getString("keyword"));

                                dto.setTagline(obj.getString("tagLine"));
                                dto.setWanted_exp(obj.getString("wanted_exp"));

                                dto.setJobId(obj.getString("jobId"));
                                System.out.println("ar=" + ar);


                                list1.add(dto);

                                System.out.println("List 1=" + list1);



                            }
                            if(list1.size()>0)
                            {
                                for (int ii = 0; ii < list1.size(); ii++) {
                                    lables1.add(list1.get(ii).getCompany_loc());
                                    lables1.add(list1.get(ii).getCompany_name());
                                    lables1.add(list1.get(ii).getInterview_loc());
                                    lables1.add(list1.get(ii).getKeyword());
                                    lables1.add(list1.get(ii).getTagline());
                                    lables1.add(list1.get(ii).getWanted_exp());
                                    System.out.println("final lable is:" + lables1);
                                    FragmentTransaction transection=getFragmentManager().beginTransaction();
                                    ApplyJobModule mfragment=new ApplyJobModule();
//using Bundle to send data
                                    Bundle bundle=new Bundle();
                                    //bundle.putStringArrayList("mylist",list1);
                                    bundle.putSerializable("array", (java.io.Serializable) list1);
                                    mfragment.setArguments(bundle); //data being send to SecondFragment
                                    transection.replace(R.id.container_body, mfragment);
                                    transection.commit();
                                }

                            }


                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else {
                    //Utility.showAlert(LoginActivity.this, null, "Invalid Response contact system admin");
                    //Utility.showAlert(SponsorsFragment.this, null, "Invali userId/password");
                }

            } else {
                //Utility.showAlert(LoginActivity.this, "Network problem", "Check your internet connection");
                // Utility.showAlert(LoginActivity.this, null, "Invali userId/password");
            }
            Utility.hideProgressDialog();
        }
    };
    private Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        paint.setColor(Color.parseColor("#BAB399"));
        canvas.drawCircle(bitmap.getWidth() / 2 + 0.7f, bitmap.getHeight() / 2 + 0.7f,
                bitmap.getWidth() / 2 + 0.1f, paint);
        //canvas.drawOval(rectF, paint);
        //canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }
}
