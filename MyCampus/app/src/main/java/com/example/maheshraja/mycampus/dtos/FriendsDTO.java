package com.example.maheshraja.mycampus.dtos;

/**
 * Created by Swamy on 3/5/2016.
 */
public class FriendsDTO implements DTO {
    /*FRIENDS_CNT
      id
      name
      mobileNo
    */
    private long id;
    private String name;
    private String mobileNum;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNo() {
        return mobileNum;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNum = mobileNo;
    }



}
