package com.example.maheshraja.mycampus.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;


/**
 * Created by Swamy on 2/25/2016.
 */
public class BaseFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    protected static GoogleApiClient mGoogleApiClient;
    protected static LocationRequest mLocationRequest;
    private final String TAG = "mGoogleApiClient";

    protected static String location;
    protected final int COUNT_DOWN_TIME = 70000;
    protected final int COUNT_DOWN_TIME_INTERVAL = 3000;

    private CountDownTimer mLocationCountDownTimer;

    protected static boolean checkForLocation = true;

    protected MainActivity mActivity;

    private ProgressDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) this.getActivity();
    }

    public void showProgressDialog() {
        dialog = new ProgressDialog(mActivity);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setMessage("Please wait...");
        dialog.show();
    }

    public void hideProgressDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API).build();
            createLocationRequest();
        }
    }

    /**
     * Creating location request object
     */
    protected void createLocationRequest() {
        if (mLocationRequest == null) {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(0);
            mLocationRequest.setFastestInterval(0);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setSmallestDisplacement(0); // 10 meters
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());

    }

    @Override
    public void onConnected(Bundle arg0) {
        if (location != null && location.length() > 0)
            return;
        try {
            Location loc = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (loc != null) {
                location = "" + loc.getLatitude() + "," + loc.getLongitude();
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }

        LocationManager lm = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);
        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            startLocationUpdates();
            mLocationCountDownTimer = new CountDownTimer(COUNT_DOWN_TIME, COUNT_DOWN_TIME_INTERVAL) {

                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    try {
                        stopLocationUpdates();
                        Location loc = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        if (loc != null) {
                            location = "" + loc.getLatitude() + "," + loc.getLongitude();
                        }
                    } catch (SecurityException e) {

                    }
                }
            };
            mLocationCountDownTimer.start();
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();

    }

    /**
     * Starting the location updates
     */
    protected void startLocationUpdates() {
        try {
            if (mGoogleApiClient.isConnected())
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } catch (SecurityException e) {

        }

    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onLocationChanged(Location loc) {
        // Assign the new location
        //mLastLocation = location;

        if (loc != null) {
            location = "" + loc.getLatitude() + "," + loc.getLongitude();
            stopLocationUpdates();
            cancelLocationTimer();
        }
    }

    private void cancelLocationTimer(){
        if(mLocationCountDownTimer != null){
            mLocationCountDownTimer.cancel();
            mLocationCountDownTimer = null;
        }
    }


    protected void getCurrentLocation(final Context context){

        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if(lm.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            if(mGoogleApiClient != null){
                if(mGoogleApiClient.isConnected()){
                    mGoogleApiClient.disconnect();
                }
                mGoogleApiClient.connect();
            }
            showProgressDialog();
            new CountDownTimer(COUNT_DOWN_TIME, COUNT_DOWN_TIME_INTERVAL) {

                @Override
                public void onTick(long millisUntilFinished) {
                    if(location != null){
                        hideProgressDialog();
                        Toast.makeText(context, MyConstants.GOT_LOCATION_MSG, Toast.LENGTH_SHORT).show();
                        this.cancel();
                    }
                }

                @Override
                public void onFinish() {
                    hideProgressDialog();
                    Toast.makeText(context, MyConstants.GOT_LOCATION_MSG, Toast.LENGTH_SHORT).show();
                    checkForLocation = false;
                    this.cancel();
                }
            }.start();
        }else{
            Utility.showAlertToOpenGPS(context);
        }

    }


}


