package com.example.maheshraja.mycampus.Activity;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

/**
 * Created by lenovo on 18-07-2016.
 */
public class MySpannable  extends ClickableSpan{
    private boolean isUnderline = true;
    public MySpannable(boolean isUnderline) {
        this.isUnderline = isUnderline;
    }

    @Override
    public void updateDrawState(TextPaint ds) {

        ds.setUnderlineText(isUnderline);

    }

    @Override
    public void onClick(View widget) {

    }
}
