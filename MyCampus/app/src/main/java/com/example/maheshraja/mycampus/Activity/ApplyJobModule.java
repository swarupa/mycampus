package com.example.maheshraja.mycampus.Activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.adapter.JobserachlistAdapter;
import com.example.maheshraja.mycampus.dtos.JobPortalDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 01-08-2016.
 */
public class ApplyJobModule  extends  BaseFragment {
    private Context context;
    List<JobPortalDTO> myStrings = null;
    List<JobPortalDTO> list1 = new ArrayList<>();
    JobserachlistAdapter adapter;
    ListView tx;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        context = mActivity;
        View rootView = inflater.inflate(R.layout.fragemt_applied_listview, container, false);
        Bundle bundle = getArguments();
        myStrings = (List<JobPortalDTO>) bundle.getSerializable("array");
        System.out.println("its mine" + myStrings);


        tx = (ListView) rootView.findViewById(R.id.list_view);
        adapter = new JobserachlistAdapter(myStrings, context);
        tx.setAdapter(adapter);


        return rootView;
    }

}