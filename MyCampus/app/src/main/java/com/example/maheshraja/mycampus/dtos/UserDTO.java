package com.example.maheshraja.mycampus.dtos;

import java.util.List;

/**
 * Created by Swamy on 3/2/2016.
 */
public class UserDTO implements DTO {
    /*USER
    id
    name
    mobilenum
    email
    gender
    address
    uid
    uidType
    dob
    location
    */
    private String name;
    private String mobilenum;
    private String mailid;
    private String gender;
    private String address;
    private String uId;
    private String uIdType;
    private String age;
    private String location;
    private String password;
    private int code;
    private List<FriendsDTO> friends;
    private String authcode;

    public String getAuthcode() {
        return authcode;
    }

    public void setAuthcode(String authcode) {
        this.authcode = authcode;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<FriendsDTO> getFriends() {
        return friends;
    }

    public void setFriends(List<FriendsDTO> friends) {
        this.friends = friends;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNo() {
        return mobilenum;
    }

    public void setMobileNo(String mobileNo) {
        this.mobilenum = mobileNo;
    }

    public String getEmailId() {
        return mailid;
    }

    public void setEmailId(String mailId) {
        this.mailid = mailId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getuIdType() {
        return uIdType;
    }

    public void setuIdType(String uIdType) {
        this.uIdType = uIdType;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
