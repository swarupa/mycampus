package com.example.maheshraja.mycampus.daos;

import android.database.sqlite.SQLiteDatabase;

import com.example.maheshraja.mycampus.dtos.DTO;

import java.util.List;

/**
 * Created by Swamy on 3/2/2016.
 */
public interface DAO {

    public boolean insert(DTO dtoObject, SQLiteDatabase dbObject);

    public boolean update(DTO dtoObject, SQLiteDatabase dbObject);

    public boolean delete(DTO dtoObject, SQLiteDatabase dbObject);

    public List<DTO> getRecords(SQLiteDatabase dbObject);

    public List<DTO> getRecordInfoByValue(String columnName, String columnValue, SQLiteDatabase dbObject);

    public DTO getRecordsById(long Id, SQLiteDatabase dbObject);

}
