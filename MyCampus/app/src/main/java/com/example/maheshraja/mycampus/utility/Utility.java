package com.example.maheshraja.mycampus.utility;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings.Secure;
import android.util.Log;
import android.widget.Toast;

import com.example.maheshraja.mycampus.Activity.MainActivity;
import com.example.maheshraja.mycampus.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/*import com.cands.techgenius.activity.MainActivity;
import com.cands.techgenius.mysafe.vizag.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;*/

public class Utility {
    private static Context context;
    protected static InputStream inputStream;

    public static String location = null;
    public static String currentItemTotalAmount = null;

    public static String userName = null;
    private static ProgressDialog dialog;


    /**
     * setting the application context
     *
     * @param context
     */


    /*public static String getIMEINo(Context context){
        StringBuilder builder = new StringBuilder();
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        builder.append(manager.getDeviceId(0));
        if(manager.getPhoneCount() == 2){
            builder.append(";");
            builder.append(manager.getDeviceId(1));
        }
        return builder.toString();
    }*/


    public static void setContext(Context context) {
        Utility.context = context;
    }

    /**
     * Getting the application context
     *
     * @return
     */
    public static Context getApplicationContxt() {
        return context;
    }

    /**
     * Displaying the Toast Message
     *
     * @param message message which you want to display
     */

    public static void displayToast(Context c, String message) {
        Toast.makeText(c, message, Toast.LENGTH_LONG).show();
    }

    /**
     * Checking the network Availability
     *
     * @return ture if network is availability in device
     */
    public static Boolean networkAvailability(Context c) {
        try {
            boolean wifiAvailability = false;
            boolean gprsAvailability = false;

            ConnectivityManager cManager = (ConnectivityManager) c
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo[] networkInfo = cManager.getAllNetworkInfo();

            for (NetworkInfo nInfo : networkInfo) {
                if (nInfo.getTypeName().equalsIgnoreCase("WIFI")) {
                    if (nInfo.isConnected())
                        wifiAvailability = true;
                }
                if (nInfo.getTypeName().equalsIgnoreCase("MOBILE")) {
                    if (nInfo.isConnected())
                        gprsAvailability = true;
                }
            }

            return wifiAvailability || gprsAvailability;
        } catch (Exception e) {
            Log.v("networkAvailability:", e.toString());
        }

        return false;
    }

    /**
     * get the value from properties file from assets directory
     *
     * @param key key for obtain value
     * @return String value
     */
    public static String getValueFromAssets(Context c, String key)
            throws IOException, Exception {

		/*
         * AssetManager assetManager = c.getAssets(); inputStream =
		 * assetManager.open(Constants.PROPERTIES_FILE_NAME); Properties
		 * properties = new Properties(); properties.load(inputStream); return
		 * properties.getProperty(key);
		 */
        return null;

    }

    /**
     * get the value from properties file from assets directory passing context
     *
     * @param key    key for obtain value
     * @param contxt context
     * @return String
     * @throws IOException
     */
    public static String getValueFromAssetsWithContext(Context c, String key,
                                                       Context contxt) throws IOException, Exception {
        /*
         * AssetManager assetManager = c.getAssets(); inputStream =
		 * assetManager.open(Constants.PROPERTIES_FILE_NAME); Properties
		 * properties = new Properties(); properties.load(inputStream); return
		 * properties.getProperty(key);
		 */
        return null;

    }

    /**
     * method will convert the Input stream to String
     *
     * @param is
     * @return String
     * @throws IOException
     * @throws Exception
     */
    public static String convertStreamToString(InputStream is)
            throws IOException, Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;

        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();
        System.out.println("String from Server===" + sb.toString());
        return sb.toString();
    }


    public static String getCurrentDate() {
        Calendar c = Calendar.getInstance();// 03/Sep/2015
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
        return sdf.format(c.getTime());
    }

    public static Date getCurrentformatedDate() {
        Calendar c = Calendar.getInstance();// 03/Sep/2015
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
        String currDate = sdf.format(c.getTime());
        Date date = null;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MMM/yyyy");
        try {
            date = formatter.parse(currDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }


    public static void showAlert(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.create().show();
    }

    public static void setDeviceId(String deviceId, Context context) {
        SharedPreferences example = context.getSharedPreferences("deviceId", 0);
        Editor editor = example.edit();
        editor.putString("deviceId", deviceId);
        editor.commit();
    }

    public static String getDeviceId(Context context) {

        SharedPreferences example = context.getSharedPreferences("deviceId", 0);
        String number = example.getString("deviceId", null);
        if (number == null) {
            number = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
            setDeviceId(number, context);
        }
        return number;
    }

    public static void setAuthCode(String authCode, Context context) {
        SharedPreferences example = context.getSharedPreferences("authCode", 0);
        Editor editor = example.edit();
        editor.putString("authCode", authCode);
        editor.commit();
    }

    public static String getAuthCode(Context context) {
        SharedPreferences example = context.getSharedPreferences("authCode", 0);
        return example.getString("authCode", null);
    }

    public static String getProfileImageURL(Context context) {
        SharedPreferences example = context.getSharedPreferences("hybridname",
                0);
        String fileDir = example.getString("hybridname", null);
        return fileDir;
    }

    public static void setProfileImageURL(String fileDir, Context context) {
        SharedPreferences example = context.getSharedPreferences("hybridname",
                0);
        Editor editor = example.edit();
        editor.putString("hybridname", fileDir);
        editor.commit();
    }

    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkPlayServices(Context context) {
        int result = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (result == ConnectionResult.SUCCESS) {
            return true;
        } else if (ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED == result) {
            Utility.showAlert(context, "", "Update google play services for better performance");
        } else if (ConnectionResult.SERVICE_MISSING == result) {
            Utility.showAlert(context, "", "google play services missing install/update for better performance");
        } else if (ConnectionResult.SERVICE_DISABLED == result) {
            Utility.showAlert(context, "", "google play services disabled enable for better performance");
        } else if (ConnectionResult.SERVICE_INVALID == result) {
            Utility.showAlert(context, "", "google play services invalid install/update for better performance");

        }

        return false;
    }

    public static void showAlertToOpenGPS(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(context.getResources().getString(R.string.gps));
        builder.setPositiveButton(context.getResources().getString(R.string.ok), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        });

        builder.setNegativeButton(context.getResources().getString(R.string.cancel), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((MainActivity) context).popFragments();
            }
        });

        builder.create().show();
    }

    public static void setFriendsComplete(boolean number, Context context) {
        SharedPreferences example = context.getSharedPreferences("appPref", 0);
        Editor editor = example.edit();
        editor.putBoolean("friends", number);
        editor.commit();
    }

    public static boolean isFriendsComplete(Context context) {
        SharedPreferences example = context.getSharedPreferences("appPref", 0);
        boolean number = example.getBoolean("friends", false);
        return number;
    }

    public static void setOTPComplete(boolean number, Context context) {
        SharedPreferences example = context.getSharedPreferences("appPref", 0);
        Editor editor = example.edit();
        editor.putBoolean("otp", number);
        editor.commit();
    }

    public static boolean isOTPComplete(Context context) {
        SharedPreferences example = context.getSharedPreferences("appPref", 0);
        boolean number = example.getBoolean("otp", false);
        return number;
    }

    public static void showProgressDialog(Context context) {
        dialog = new ProgressDialog(context);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setMessage(context.getResources().getString(R.string.progress_pleaseWait));
        dialog.show();
        long delayInMillis = 7000;
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, delayInMillis);
    }

    public static void hideProgressDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public static void setFileDir(String fileDir, Context context)
    {
        SharedPreferences example = context.getSharedPreferences("fileDir", 0);
        Editor editor = example.edit();
        editor.putString("fileDir", fileDir);
        editor.commit();
    }

    public static String getFileDir(Context context)
    {
        SharedPreferences example = context.getSharedPreferences("fileDir", 0);
        String fileDir = example.getString("fileDir", null);
        return fileDir;
    }

    public static void setFirst(boolean reqId, Context context)
    {
        SharedPreferences sp = context.getSharedPreferences("vpcs", 0);
        Editor editor = sp.edit();
        editor.putBoolean("first", reqId);
        editor.commit();
    }

    public static void removeFirst(Context context)
    {
        SharedPreferences sp = context.getSharedPreferences("vpcs", 0);
        Editor editor = sp.edit();
        editor.remove("first");
        editor.commit();
    }

    public static boolean isFirst(Context context)
    {
        SharedPreferences sp = context.getSharedPreferences("vpcs", 0);
        return sp.getBoolean("first", true);
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}