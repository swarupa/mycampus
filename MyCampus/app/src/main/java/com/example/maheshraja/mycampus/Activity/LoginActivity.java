package com.example.maheshraja.mycampus.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.asynctask.MyAsyncTask;
import com.example.maheshraja.mycampus.daos.JobportalDAo;
import com.example.maheshraja.mycampus.daos.UserDAO;
import com.example.maheshraja.mycampus.database.DBHandler;
import com.example.maheshraja.mycampus.dtos.JobPortalDTO;
import com.example.maheshraja.mycampus.dtos.LoginDataDTO;
import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;


public class LoginActivity extends AppCompatActivity {

    private TextView txtCreateAC;
    private TextInputLayout userNameLayout, pwdLayout;
    private EditText edtUserName, edtPwd;
    private Button btnSubmit;

    private Context context;

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);

        context = this;

        txtCreateAC = (TextView) findViewById(R.id.txt_create_ac);

        userNameLayout = (TextInputLayout) findViewById(R.id.login_layout_loginId);
        pwdLayout = (TextInputLayout) findViewById(R.id.login_layout_password);

        edtUserName = (EditText) findViewById(R.id.login_loginId);
        edtPwd = (EditText) findViewById(R.id.login_password);

        btnSubmit = (Button) findViewById(R.id.login_submit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(validateFields()) {
                    /*Intent intent = new Intent(context, MainActivity.class);
                    startActivity(intent);*/
                    if (Utility.networkAvailability(context)) {

                        loginProcess();

                    } else {
                        Utility.showAlert(context, null, getString(R.string.check_connection));
                    }
                }
            }
        });

        txtCreateAC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }

    private boolean validateFields(){
        if(isTextEmpty(edtUserName)) {
            userNameLayout.setError("Enter Mail Id");
            return false;
        }

        if(isTextEmpty(edtPwd)) {
            pwdLayout.setError("Enter Password");
            return false;
        }

        return true;
    }

    private boolean isTextEmpty(EditText editText){
        if(editText.getText().toString().trim().length() > 0)
            return false;
        else
            return true;
    }

    private void loginProcess() {
        String reqData = generateLoginReqData();
        MyAsyncTask loginAsync = new MyAsyncTask(reqData, completeListener, MyConstants.CONNECTION_TIMEOUT, MyConstants.SOCKET_TIMEOUT);
        Utility.showProgressDialog(context);
        loginAsync.execute(MyConstants.LOGIN_URL);
    }

    private String generateLoginReqData(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("mailid", getText(edtUserName));
            jsonObject.put("password", getText(edtPwd));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return jsonObject.toString();
    }

    private String getText(EditText editText){
        return editText.getText().toString().trim();
    }

    private AsyncTaskCompleteListener completeListener = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result) {
            if (result != null && !result.isEmpty()) {
                if(Utility.isJSONValid(result)){
                    try {
                       Gson gson =  new Gson();
                        System.out.println("seeker result"+result);
                        LoginDataDTO dto = gson.fromJson(result, LoginDataDTO.class);

                        System.out.println("seeker dto"+dto);


                        if(dto.getCode() == 100){
                            System.out.println("seeker dto");
                            Utility.setAuthCode(dto.getUser().getAuthcode(), LoginActivity.this);
                            UserDAO.getInstance().insert(dto.getUser(), DBHandler.getInstance(context).getDBObject(1));

                          JobPortalDTO friendsDTOs=dto.getSeekerdetails();

                                        System.out.println("seeker details"+friendsDTOs);
                                        JobportalDAo.getInstance().insert(friendsDTOs, DBHandler.getInstance(context).getDBObject(1));


                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        finish();
                        startActivity(intent);




                        }else if(dto.getCode() == 102){
                            Utility.showAlert(LoginActivity.this, null, "Invalid credentials");
                        }else if(dto.getCode() == 101){
                            Utility.showAlert(LoginActivity.this, null, "Invalid credentials");
                        }else{
                            Utility.showAlert(LoginActivity.this, null, "Unknown response");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    //Utility.showAlert(LoginActivity.this, null, "Invalid Response contact system admin");
                    Utility.showAlert(LoginActivity.this, null, "Invali userId/password");
                }

            } else {
                //Utility.showAlert(LoginActivity.this, "Network problem", "Check your internet connection");
                Utility.showAlert(LoginActivity.this, null, "Invali userId/password");
            }
            Utility.hideProgressDialog();
        }
    };


}
