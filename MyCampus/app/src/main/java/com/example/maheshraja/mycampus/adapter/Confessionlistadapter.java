package com.example.maheshraja.mycampus.adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.maheshraja.mycampus.Activity.ExpandableTextView;
import com.example.maheshraja.mycampus.Activity.MainActivity;
import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.asynctask.MyAsyncTask;
import com.example.maheshraja.mycampus.daos.UserDAO;
import com.example.maheshraja.mycampus.database.DBHandler;
import com.example.maheshraja.mycampus.dtos.ConfessionDTO;
import com.example.maheshraja.mycampus.dtos.DTO;
import com.example.maheshraja.mycampus.dtos.UserDTO;
import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by lenovo on 16-07-2016.
 */
public class Confessionlistadapter extends BaseAdapter {
    private List<ConfessionDTO> list;
    ConfessionDTO dto;
    Context context;
    String college_name;
    String id;
    String strs;
    String str1;
    String msg1;
    String url = null;
    int i;
    EditText msg;
     Dialog dialog=null;
    String count;
    String likecount;
    protected MainActivity mActivity;
     JSONObject object = null;

    public Confessionlistadapter(List<ConfessionDTO> list, Context context) {
        this.list = list;
        this.context = context;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View view = LayoutInflater.from(context).inflate(R.layout.fragment_demo1, null);
        dto = list.get(position);

        final TextView text = (TextView) view.findViewById(R.id.subtextview);
        final TextView text1 = (TextView) view.findViewById(R.id.subtextview1);
        final TextView text2 = (TextView) view.findViewById(R.id.likecount);
        ExpandableTextView expTv1 = (ExpandableTextView) view.findViewById(R.id.sample1)
                .findViewById(R.id.expand_text_view);
        ExpandableTextView expTv2 = (ExpandableTextView) view.findViewById(R.id.sample2)
                .findViewById(R.id.expand_text_view);

        expTv1.setOnExpandStateChangeListener(new ExpandableTextView.OnExpandStateChangeListener() {
            @Override
            public void onExpandStateChanged(TextView textView, boolean isExpanded) {
                Toast.makeText(context, isExpanded ? "Expanded" : "Collapsed", Toast.LENGTH_SHORT).show();
            }
        });

        String name = dto.getData();
        System.out.println("message is:" + name);
        text1.setText("Confession #" + dto.getConfession_id());

        text.setText(dto.getCollege_name());
        expTv1.setText(dto.getData());
        int finallikecount = dto.getLikecount();
        System.out.println("final count is:"+finallikecount);

        text2.setText(Integer.toString(dto.getLikecount()));

        ImageView btncomment = (ImageView) view.findViewById(R.id.commentid);
        //btnVideo.setOnClickListener();
        btncomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.commentid:
                        college_name = text.getText().toString().trim();
                        id = text1.getText().toString().trim();
                        StringTokenizer str = new StringTokenizer(id, "#");
                        while (str.hasMoreElements()) {
                            strs = str.nextToken();
                            System.out.println("first token is:" + str);
                            str1 = str.nextToken();
                            System.out.println("first token is:" + str1);

                        }
                        i = Integer.parseInt(str1);
                        System.out.println("rameshswarupa is :" + college_name);
                        System.out.println("rameshswarupa is  id:" + i);
                        dialog = new Dialog(context);

                        dialog.setContentView(R.layout.comment);
                        dialog.show();


                        final EditText msg = (EditText) dialog.findViewById(R.id.alert_msg);
                        Button btnYes = (Button) dialog.findViewById(R.id.alert_yes);
                        Button btnNo = (Button) dialog.findViewById(R.id.alert_no);
                        btnYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                msg1 = getRequestData1(msg.getText().toString().trim());

                                msg.getText().toString().trim();
                                StringTokenizer str = new StringTokenizer(id, "#");
                                while (str.hasMoreElements()) {
                                    strs = str.nextToken();
                                    System.out.println("first token is:" + str);
                                    str1 = str.nextToken();
                                    System.out.println("first token is:" + str1);

                                }
                                i = Integer.parseInt(str1);

                                System.out.println("comment is  :" + msg);
                                System.out.println(" final comment is  :" + msg1);
                                //Toast.makeText(context, "Your Commen", Toast.LENGTH_SHORT).show();


                            }
                        });

                        btnNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.cancel();
                            }
                        });


                }
            }


        });

        final ImageView btnLike = (ImageView) view.findViewById(R.id.likeid);
        //btnVideo.setOnClickListener();
        btnLike.setOnClickListener(new View.OnClickListener() {

            /* @Override
                        public void onClick(View v) {
                            int count=0;
                            count++;
                            text2.setText(String.valueOf(count));

                          *//*  int count=0;
                if(v.getTag()==null)
                {
                    count=0;
                }

                else
                {
                    Integer.parseInt(v.getTag().toString());
                }
                count++;
           text2.setText("Like:"+count++);
                v.setTag(count);*//*
             *//*  switch (v.getId()) {
                    case R.id.likeid:


                        college_name = text.getText().toString().trim();
                        id = text1.getText().toString().trim();
                        System.out.println("id is:"+id);

                        StringTokenizer str = new StringTokenizer(id, "#");
                        while (str.hasMoreElements()) {
                            strs = str.nextToken();
                            System.out.println("first token is:" + str);
                            str1 = str.nextToken();
                            System.out.println("first token is:" + str1);

                        }
                        i = Integer.parseInt(str1);

                        Toast.makeText(context, "like clicked" + college_name, Toast.LENGTH_SHORT).show();
                        senddata();

                        break;

                }*//*



            }
        });

*/
            @Override
            public void onClick(View v) {
                /*int count=0;
                count = Integer.parseInt((String)text2.getText());
                count++;
                text2.setText( ""+count);*/
                boolean handledClick = false;
                if (v.getId()==R.id.likeid) {
                btnLike.setEnabled(false);
                    college_name = text.getText().toString().trim();
                    id = text1.getText().toString().trim();
                    StringTokenizer str = new StringTokenizer(id, "#");
                    while (str.hasMoreElements()) {
                        strs = str.nextToken();
                        System.out.println("first token is:" + str);
                        str1 = str.nextToken();
                        System.out.println("first token is:" + str1);

                    }
                    i = Integer.parseInt(str1);

                    String present_value_string = text2.getText().toString();
                    int present_value_int = Integer.parseInt(present_value_string);
                    present_value_int++;

                    text2.setText(String.valueOf(present_value_int));
                    likecount = text2.getText().toString().trim();
                    senddata();

                }
                else
                {
                    btnLike.setEnabled(true);
                    btnLike.setClickable(false);
                }
            }





        });
        return view;
    }




    private String getRequestData1(String msg) {



        ConfessionDTO dto = (ConfessionDTO) getUserData();
        UserDTO user = (UserDTO) UserDAO.getInstance().getRecords(DBHandler.getInstance(context).getDBObject(0)).get(0);
        JSONObject reqJson = new JSONObject();

        try {
            reqJson.put("college_name",college_name);
            reqJson.put("confession_id",i);
            reqJson.put("user_name", user.getEmailId());
            reqJson.put("user_comment",msg);

        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){

        }

        MyAsyncTask loginAsync = new MyAsyncTask(reqJson.toString(), taskCompleteListener, MyConstants.CONNECTION_TIMEOUT, MyConstants.SOCKET_TIMEOUT);
        //Utility.showProgressDialog(context);
        loginAsync.execute(MyConstants.Confession_comment_URL);
        return reqJson.toString();


    }


    private String getRequestData(String msg) {


        ConfessionDTO dto = (ConfessionDTO) getUserData();
        UserDTO user = (UserDTO) UserDAO.getInstance().getRecords(DBHandler.getInstance(context).getDBObject(0)).get(0);
        JSONObject reqJson = new JSONObject();

        try {
            reqJson.put("college_name",college_name);
            reqJson.put("confession_id",i);
            reqJson.put("user_name", user.getEmailId());

        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){

        }

        MyAsyncTask loginAsync = new MyAsyncTask(reqJson.toString(), taskCompleteListener, MyConstants.CONNECTION_TIMEOUT, MyConstants.SOCKET_TIMEOUT);
        //Utility.showProgressDialog(context);
        loginAsync.execute(MyConstants.Confession_LIKE_URL);
        return reqJson.toString();
    }
    private void senddata()
    {
        ConfessionDTO dto = (ConfessionDTO) getUserData();
        UserDTO user = (UserDTO) UserDAO.getInstance().getRecords(DBHandler.getInstance(context).getDBObject(0)).get(0);
        JSONObject reqJson = new JSONObject();

        try {
            reqJson.put("college_name",college_name);
            reqJson.put("confession_id",i);
            reqJson.put("user_name", user.getEmailId());
            reqJson.put("likecount",likecount);
        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){

        }

        MyAsyncTask loginAsync = new MyAsyncTask(reqJson.toString(), taskCompleteListener1, MyConstants.CONNECTION_TIMEOUT, MyConstants.SOCKET_TIMEOUT);
        //Utility.showProgressDialog(context);
        loginAsync.execute(MyConstants.Confession_LIKE_URL);

    }

    private DTO getUserData() {

        ConfessionDTO dto = new ConfessionDTO();
        dto.setCollege_name(college_name);
        dto.setConfession_id(i);

        return  dto;
    }


    private AsyncTaskCompleteListener taskCompleteListener = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result1) {
            if (result1 != null) {
                if(Utility.isJSONValid(result1)){
                    try {
                        System.out.println("test AsyncTask for insert");
                     object = new JSONObject(result1);
                        int code = object.optInt("code");
                        if(code == 100){

                           //Toast.makeText(context, "like clicked" , Toast.LENGTH_SHORT).show();
                            dialog.cancel();
                        

                        }else if(code == 101){
                            Utility.showAlert(context, null, "Failed to Insert");
                        }else{
                            Utility.showAlert(context, null, "Unknown response");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Utility.showAlert(context, null, "Invalid Response contact system admin");
                }

            } else {
                Utility.showAlert(context, "Network problem", "Check your internet connection");
            }
            Utility.hideProgressDialog();
        }
    };



    private AsyncTaskCompleteListener taskCompleteListener1 = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result1) {
            if (result1 != null) {
                if(Utility.isJSONValid(result1)){
                    try {
                        System.out.println("test AsyncTask for insert");
                        object = new JSONObject(result1);
                        int code = object.optInt("code");
                        if(code == 100){

                            //Toast.makeText(context, "like clicked" , Toast.LENGTH_SHORT).show();
                            //dialog.cancel();


                        }else if(code == 101){
                            Utility.showAlert(context, null, "Failed to Insert");
                        }else{
                            Utility.showAlert(context, null, "Unknown response");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Utility.showAlert(context, null, "Invalid Response contact system admin");
                }

            } else {
                Utility.showAlert(context, "Network problem", "Check your internet connection");
            }
            Utility.hideProgressDialog();
        }
    };


}


