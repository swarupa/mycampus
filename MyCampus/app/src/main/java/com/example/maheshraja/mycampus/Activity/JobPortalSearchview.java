package com.example.maheshraja.mycampus.Activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.maheshraja.mycampus.R;
import com.example.maheshraja.mycampus.adapter.JobserachlistAdapter;
import com.example.maheshraja.mycampus.asynctask.SearchJobAsynTask;
import com.example.maheshraja.mycampus.daos.JobportalDAo;
import com.example.maheshraja.mycampus.database.DBHandler;
import com.example.maheshraja.mycampus.dtos.DTO;
import com.example.maheshraja.mycampus.dtos.JobPortalDTO;
import com.example.maheshraja.mycampus.listener.AsyncTaskCompleteListener;
import com.example.maheshraja.mycampus.utility.MyConstants;
import com.example.maheshraja.mycampus.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 30-07-2016.
 */
public class JobPortalSearchview  extends  BaseFragment {

    private Context context;
    List<JobPortalDTO> myStrings = null;
    List<JobPortalDTO> list1 = new ArrayList<>();
    JobserachlistAdapter adapter;
    String jobId;
Button button;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        context = mActivity;
        View rootView = inflater.inflate(R.layout.fragment_list_jobsearch, container, false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            String CompanyName = getArguments().getString("company_name");
            System.out.println(CompanyName);

            String CompanyLoc = getArguments().getString("company_loc");
            System.out.println(CompanyLoc);

            String tagline = getArguments().getString("tagline");
            System.out.println(tagline);

            String keyword = getArguments().getString("keyword");
            System.out.println(keyword);

            String wantedexp = getArguments().getString("wanted_exp");
            System.out.println(wantedexp);

            String interview_loc = getArguments().getString("Interview_loc");
            System.out.println(interview_loc);


            String jobdescription = getArguments().getString("jobdescription");
            System.out.println(jobdescription);

           jobId = getArguments().getString("jobid");
            System.out.println(jobdescription);

            // String CompanyName=getArguments().getString("company_name");
            final TextView text = (TextView) rootView.findViewById(R.id.subtextview0);
            final TextView text1 = (TextView) rootView.findViewById(R.id.subtextview1);
            final TextView text2 = (TextView) rootView.findViewById(R.id.subtextview2);
            final TextView text3 = (TextView) rootView.findViewById(R.id.subtextview3);
            final TextView text4 = (TextView) rootView.findViewById(R.id.subtextview4);
            final TextView text5 = (TextView) rootView.findViewById(R.id.subtextview5);

            text.setText(tagline);
            text1.setText(CompanyName);
            text2.setText(CompanyLoc);
            text3.setText(keyword);
            text4.setText(wantedexp);

            ExpandableTextView expTv1 = (ExpandableTextView) rootView.findViewById(R.id.Sample0)
                    .findViewById(R.id.expand_text_view);


            expTv1.setOnExpandStateChangeListener(new ExpandableTextView.OnExpandStateChangeListener() {
                @Override
                public void onExpandStateChanged(TextView textView, boolean isExpanded) {
                    Toast.makeText(context, isExpanded ? "Expanded" : "Collapsed", Toast.LENGTH_SHORT).show();
                }
            });
            expTv1.setText(jobdescription);

            button = (Button) rootView.findViewById(R.id.submit);



            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utility.networkAvailability(context)) {

                        sendData();

                    } else {
                        Utility.showAlert(context, null, getString(R.string.check_connection));
                    }
                }
            });



        }
        return rootView;

    }


    private void sendData() {

        JobPortalDTO dto = (JobPortalDTO) getUserData();
        JobPortalDTO user = (JobPortalDTO) JobportalDAo.getInstance().getRecords(DBHandler.getInstance(context).getDBObject(0)).get(0);
        JSONObject reqJson = new JSONObject();

        try {
            reqJson.put("jobId", dto.getJobId());
            reqJson.put("username", user.getEmailAddress());
            System.out.println("username" + user.getEmailAddress());
            System.out.println("jobId"+dto.getJobId());
        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){

        }

        SearchJobAsynTask loginAsync = new SearchJobAsynTask(reqJson.toString(), taskCompleteListener, MyConstants.CONNECTION_TIMEOUT, MyConstants.SOCKET_TIMEOUT);
        Utility.showProgressDialog(context);
        loginAsync.execute(MyConstants.JOBSEARCH_APPLY_URL);

    }
    private DTO getUserData() {

        JobPortalDTO dto = new JobPortalDTO();
        dto.setJobId(jobId);

        return  dto;
    }
    private AsyncTaskCompleteListener taskCompleteListener = new AsyncTaskCompleteListener() {
        @Override
        public void onAsynComplete(String result1) {
            if (result1 != null) {
                if(Utility.isJSONValid(result1)){
                    try {
                        System.out.println("test AsyncTask for insert");
                        JSONObject object = new JSONObject(result1);
                        int code = object.optInt("code");
                        if(code == 100){
                            //navigateApp();
                            //clearForm();
                            Utility.showAlert(context, null, "Job Apply  Sucessfully");
                        }else if(code == 101){
                            Utility.showAlert(context, null, "Failed to Insert");
                        }else{
                            Utility.showAlert(context, null, "Unknown response");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Utility.showAlert(context, null, "Invalid Response contact system admin");
                }

            } else {
                Utility.showAlert(context, "Network problem", "Check your internet connection");
            }
            Utility.hideProgressDialog();
        }
    };


}